#include "magnetspywidget.h"
#include <QList>
#include <QString>
#include <QTableWidget>
#include <QtGlobal>
#include "QCryptographicHash"
#include "arpmanetdc.h"
#include "transfermanager.h"


//MagnetspyWidget::MagnetspyWidget()
//{}
MagnetspyWidget::MagnetspyWidget(sqlite3 *db,QList<QString> *magnetlist ,QList<QString> *regex , ArpmanetDC *parent)
{
    pParent = parent;
    pDB = db ;
    MagnetSpyList =  magnetlist ;
    pRegex =regex;

    //======Actions======

    downloadAction = new QAction(QIcon(":/ArpmanetDC/Resources/QueueIcon.png"), tr("Download File"), this);

    //=====Menus========
    magnetspyMenu = new QMenu((QWidget *)pParent);
//    finishedMenu = new QMenu(finishedTable);
//    finishedMenu->addAction(openAction);
//    finishedMenu->addAction(deleteAction);

    createWidgets();
    loadList();
    placeWidgets();
    connectWidgets();

}

MagnetspyWidget::~MagnetspyWidget()
{
}

void MagnetspyWidget::createWidgets()
{

    pWidget = new QWidget((QWidget *)pParent);

    pwatchList = new QListWidget(pWidget);

    pAdd = new QPushButton("Add",pWidget);
    premove = new QPushButton("Remove",pWidget);
    pnewName = new QLineEdit("",pWidget);
    pnewName->setPlaceholderText("Enter new regex to watch for");

    pmagnetTableWidget= new QTableWidget(pWidget) ;

    pmagnetTableWidget->setRowCount(0);
    pmagnetTableWidget->setColumnCount(5);

    pmagnetTableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Name"));
    pmagnetTableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Count"));
    pmagnetTableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Size"));
    pmagnetTableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Magnet"));
    pmagnetTableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Size"));
    pmagnetTableWidget->hideColumn(3); //hide th magnet column
}

void MagnetspyWidget::placeWidgets()
{

    pmagnetTableWidget->setColumnWidth(0,400);
    pmagnetTableWidget->setColumnWidth(1,80);
    pmagnetTableWidget->setColumnWidth(2,90);
    pmagnetTableWidget->setColumnWidth(3,600);
    pmagnetTableWidget->setColumnWidth(4,400);


    pAdd->setMaximumWidth(200);
    premove->setMaximumWidth(200);
    pnewName->setMaximumWidth(200);
    pwatchList->setMaximumWidth(200);

    QVBoxLayout *modlistlayout = new QVBoxLayout;
    modlistlayout->addWidget( pAdd );
    modlistlayout->addWidget( premove );
    modlistlayout->addWidget( pnewName );
    // contains the list and the list edit widget
    QVBoxLayout *listlayout = new QVBoxLayout;
    listlayout->addWidget(pwatchList);
    listlayout->addLayout(modlistlayout);
//    QSpacerItem *item = new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed);
//    listlayout->addItem(item);
    //Main layout
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(pmagnetTableWidget);
    layout->addLayout(listlayout);

    /*
     qDebug() << "pWidget" << pWidget << "\nlayout" << layout
             << "\npmagnetTableWidget"<< pmagnetTableWidget
             << "\npmagnetTableWidget->parent()"<<pmagnetTableWidget->parent()  ;//*/

    pWidget->setLayout( layout);

}

void MagnetspyWidget::connectWidgets()
{
    connect(pAdd,SIGNAL(clicked()),this,SLOT(AddClicked()));
    connect(premove,SIGNAL(clicked()),this,SLOT(removeClicked()));

    connect(pmagnetTableWidget,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(resultDoubleClicked(QModelIndex)));
}

void MagnetspyWidget::resultDoubleClicked(QModelIndex index)
{
//    qDebug() << "void MagnetspyWidget::resultDoubleClicked(QModelIndex index)" << index;
    downloadActionPressed();
}

void MagnetspyWidget::AddClicked()
{
    qDebug() << "Clicked add";
    pRegex->append(pnewName->text());
    updateRegexwidget();

        QByteArray query ;
        sqlite3_stmt *statement;
    //    query.append(QString("INSERT INTO regex([regex]) VALUES([lol]);"));
        query.append(QString("INSERT INTO regex(regex) VALUES(\""+pnewName->text()+"\");"));

        query.append(QString("COMMIT;"));
        if (sqlite3_prepare_v2(pDB, query.data(), -1, &statement, 0) == SQLITE_OK)
        {
            int cols = sqlite3_column_count(statement);
            int result = 0;
            result = sqlite3_step(statement);

        }
}

void MagnetspyWidget::removeClicked()
{
    qDebug() << "Clicked remove";
    QString stemp;
    if(pwatchList->selectedItems().count() > 0)
    {
        qDebug() << "pwatchList->count()" << pwatchList->count() << "pwatchList->selectedItems().at(0)" << pwatchList->selectedItems().at(0);
        stemp = (pwatchList->selectedItems().at(0)->text()) ;
        pRegex->removeAt(pRegex->indexOf( stemp ));
    }
        updateRegexwidget();

        //"DELETE FROM regex WHERE regex=\""+*+"\" ;"
        QByteArray query ;
        sqlite3_stmt *statement;

        query.append(QString("DELETE FROM regex WHERE regex=\""+stemp+"\" ;"));

        query.append(QString("COMMIT;"));
        if (sqlite3_prepare_v2(pDB, query.data(), -1, &statement, 0) == SQLITE_OK)
        {
            int cols = sqlite3_column_count(statement);
            int result = 0;
            result = sqlite3_step(statement);

        }

}

QWidget *MagnetspyWidget::widget()
{
//    qDebug() << pWidget;
   return pWidget;
}

void MagnetspyWidget::loadList()
{
    updateRegexwidget();
   //MagnetSpyMap.
    MagnetSpyMap = new QMap<QString, int>;
    MagnetSpyMap->clear();
    QString stemp ;
    for(int i = MagnetSpyList->length() ; i >= 0 ; i=i-1 )
    {
        if(MagnetSpyList->value(i) !="")
        {
            stemp = (MagnetSpyList->value(i)) ;
            MagnetSpyMap->insert(stemp,MagnetSpyMap->value(stemp)+ 1)  ;
        }

//        qDebug() << "loadList()" << stemp << MagnetSpyMap->value(stemp);
        //pmagnetListwidget->addItem(MagnetSpyList->value(i));
    }
    if(MagnetSpyMap->count() > 0)
    {
        QMapIterator<QString, int> i(*MagnetSpyMap);
        int j=0;
        while (i.hasNext())
        {

            //j = pmagnetTableWidget->rowCount()+1 ;
//            j = 1 ;
            i.next();
            pmagnetTableWidget->setRowCount(pmagnetTableWidget->rowCount()+1);

            QTableWidgetItem *newItem = new QTableWidgetItem(i.key());
            pmagnetTableWidget->setItem(j, 3, newItem);
            pmagnetTableWidget->setItem(j, 1, new QTableWidgetItem(QString("%1").arg(i.value())));
            pmagnetTableWidget->setItem(j, 0, new QTableWidgetItem(extractName(i.key())));
            pmagnetTableWidget->setItem(j, 2, new QTableWidgetItem(extractSize(i.key())));
            pmagnetTableWidget->setItem(j, 4, new QTableWidgetItem(extractTth(i.key())));
   //         qDebug() <<"j" << j << i.key() << ": " << i.value() << endl;
            j++;
        }
    }
}

void MagnetspyWidget::updateRegexwidget()
{
    //QList<QString> *pRegex;

    pwatchList->clear();
    for(int i = 0 ; i < pRegex->count() ; i++)
    {
        pwatchList->addItem(new QListWidgetItem(pRegex->at(i)));
    }
}

QString MagnetspyWidget::extractName(QString magnet)
{
    int ipos1 , ipos2 ;
    ipos1 = magnet.indexOf("&dn=");
    magnet = magnet.remove(0, ipos1 +4) ;
//    qDebug() << "QString MagnetspyWidget::extractName(QString magnet)" << magnet;
    return magnet ;
}

QString MagnetspyWidget::extractSize(QString magnet)
{
    int ipos1 , ipos2 ;
    ipos1 = magnet.indexOf("&xl=");

    magnet = magnet.remove(0, ipos1 +4) ;
    ipos2 = magnet.indexOf("&dn=");
    magnet = magnet.remove(ipos2,100) ;
    bool ok ;
    qreal dec = magnet.toDouble(&ok);

    if(ok==true)
    {

        if (dec > 1024*1024*1024)
        {

            dec =dec /(1024*1024*1024);
            magnet = QString::number(dec, 'f', 2) + " GB";

        }
        else
            if (dec > 1024*1024)
            {
                dec =dec /(1024*1024);
                magnet = QString::number(dec, 'f', 2) + " MB";
            }
            else
                if (dec > 1024)
                {
                    dec =dec /(1024);
                    magnet = QString::number(dec, 'f', 2) + " KB";
                    qDebug()<<"KB";
                }
        else
                    magnet.append(" B");

    }
//    qDebug() << "QString MagnetspyWidget::extractName(QString magnet)" << magnet;
    return magnet ;

}

QString MagnetspyWidget::extractType(QString magnet)
{

}

QString MagnetspyWidget::extractTth(QString magnet)
{
    QString stemp = magnet ;
    int ipos1 , ipos2 ;

    ipos1 = stemp.indexOf(":tiger:");
    ipos1 += 7;

    ipos2 = stemp.indexOf("&",ipos1+1);

    stemp.truncate(ipos2);
    stemp.remove(0,ipos1);

    return stemp ;
}

void MagnetspyWidget::addMagnet(QString magnet)
{
//    qDebug() << "void MagnetspyWidget::addMagnet(QString magnet)" << magnet;
    //pmagnetListwidget->addItem(magnet);

}

void MagnetspyWidget::downloadActionPressed()
{
//pParent;
    QString Stemp ,Stemp2;
    //Stemp = pmagnetListwidget->selectedIndexes();
   // Stemp = pmagnetListwidget->selectedItems().
    //Get default path from parent
    QString path = ArpmanetDC::settingsManager()->getSetting(SettingsManager::DOWNLOAD_PATH);
    if (path.right(1).compare("/") != 0)
        path.append("/");

    QueueStruct item;
    int ipos1 , ipos2 ;
//    Stemp2 = "magnet:?xt=urn:tree:tiger:2ZSOXNLOR37HYXSHCSJVBB7SEJ2P7RKKNWPUSTA&xl=339922036&dn=Beauty.and.the.Beast.2012.S01E03.com.avi";
      // Stemp2 = pmagnetListwidget->currentItem()->text();

       //pmagnetTableWidget->currentRow()

    Stemp2 = pmagnetTableWidget->item(pmagnetTableWidget->currentRow(),3)->text();


    Stemp2.remove(0,26);
//    qDebug() << "Stemp2:" << Stemp2 ;
    QString s1 ,s2 ,s3 ;
    s1 = Stemp2;
    s1.remove(s1.indexOf("&xl") ,9999);

    //just use these values instead
    ipos1 = Stemp2.indexOf("&xl=");
    ipos2 = Stemp2.indexOf("&dn=");
    s2 = Stemp2;
    s2.remove(ipos2,999);
    s2.remove(0,ipos1+4);
    ipos1 = Stemp2.indexOf("&dn=");
    s3 = Stemp2 ;
    s3.remove(0,ipos1+4);
//    qDebug() << "s1:" << s1 ;
//    qDebug() << "s2:" << s2 ;
//    qDebug() << "s3:" << s3 ;
    QByteArray tthRoot;
    QString tthBase32 = s1 ;
     tthRoot.append(tthBase32);
     base32Decode(tthRoot);
     //tthRoot.append ();
    // QByteArray tthRoot();


    //QList<QString> list = pmagnetListwidget->selectedItems();

bool *ok = false ;
    item.tthRoot = tthRoot ;
    item.fileName = s3;
    item.filePath = path+s3 ;
    item.fileSize = s2.toLongLong(ok,10) ;
    item.fileHost = "0.0.0.0";
    item.priority = NormalQueuePriority ;
    pParent->addDownloadToQueue(item);
//    qDebug() << item.tthRoot << item.fileHost << item.fileName << item.fileSize << item.priority;
//emit queueDownload((int)item.priority, tthRoot, finalPath, fileSize, senderIP);
    emit queueDownload( NormalQueuePriority , tthRoot, item.filePath , item.fileSize , item.fileHost );

}

void MagnetspyWidget::updateList(QList<QString> *magnetlist )
{
//    MagnetSpyList->clear();
//    MagnetSpyList =  magnetlist ;
}
