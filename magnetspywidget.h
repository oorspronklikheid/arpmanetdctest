/* This file is part of ArpmanetDC. Copyright (C) 2012
* Source code can be found at http://code.google.com/p/arpmanetdc/
*
* ArpmanetDC is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ArpmanetDC is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ArpmanetDC.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAGNETSPYWIDGET_H
#define MAGNETSPYWIDGET_H
//#include <magnetspywidget.cpp>
#include <QtGui>
#include <QList>
#include <QString>
#include <QHostAddress>
//#include <customtableitems.h>


class ArpmanetDC;
class TransferManager;
struct sqlite3;

class MagnetspyWidget : public QObject
{
   Q_OBJECT

public:
    //MagnetspyWidget();
    MagnetspyWidget(sqlite3 *db, QList<QString> *magnetlist, QList<QString> *regex, ArpmanetDC *parent);
    ~MagnetspyWidget();

    //Get the encapsulating widget
    QWidget *widget();
    void updateList(QList<QString> *magnetlist );

signals:
    //Queue download
    void queueDownload(int priority, QByteArray tth, QString finalPath, quint64 fileSize, QHostAddress senderIP);

public slots:
   void addMagnet(QString magnet);
private slots:
   void downloadActionPressed();
   //User double clicked on a result
   void resultDoubleClicked(QModelIndex index);
   void AddClicked();
   void removeClicked();
private:
   //Functions
   void createWidgets();
   void placeWidgets();
   void connectWidgets();
   void loadList();


   //Objects
   sqlite3 *pDB;

   QWidget *pWidget;
   ArpmanetDC *pParent;

   QTableWidget *pmagnetTableWidget; //use this instead
   QListWidget *pwatchList;
   QList<QString> *pRegex;
   void updateRegexwidget();


   QMap<QString, int> *MagnetSpyMap ; //use this insted
   QList<QString>    *MagnetSpyList;  //remove this one

   QStandardItemModel *MagnetspyModel;

   QAction *downloadAction ;
   QMenu *magnetspyMenu ;

   //modify list items
   QPushButton *pAdd;
   QPushButton *premove;
   QLineEdit   *pnewName;

   QString extractName(QString magnet);
   QString extractSize(QString magnet);
   QString extractType(QString magnet);
   QString extractTth(QString magnet);

};

#endif // MAGNETSPYWIDGET_H
